#!/usr/bin/env bash

echo "Installing Node.js"
curl -s http://nodejs.org/dist/latest/node-v0.10.33-linux-x64.tar.gz | tar xzf - --strip-components=1 --directory /usr/local

echo "Installing FFmpeg"
cd /tmp
wget --no-verbose "http://johnvansickle.com/ffmpeg/releases/ffmpeg-2.4.1-64bit-static.tar.xz"
tar xf ffmpeg-*-64bit-static.tar.xz
cp ffmpeg*/ffmpeg /usr/local/bin/
