var child_process = require('child_process');
var express = require('express');
var jade = require('jade');
var path = require('path');
var winston = require('winston');

//Set up app log
global.applog = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      colorize: true,
      timestamp: true,
      level: 'debug',
      handleExceptions: true
    })
  ]
});

applog.info('Radio Reddit Media Transcoder starting up');

applog.debug('Loading encoder profiles');
var profiles = require(__dirname + '/conf/profiles.js');

applog.debug('Loading config');
var config = require(__dirname + '/conf/config.json');
config = config || {};
config.server = config.server || {};
config.server.port = config.server.port || 80;
config.server.bindAddress = config.server.bindAddress || '0.0.0.0';

var app = express();
app.engine('jade', jade.__express);
app.locals['package'] = require(__dirname + '/package.json');

app.use(function (req, res, next) {
  applog.info('HTTP Request', {method: req.method, url: req.url});
  return next();
});

app.get(/\/transcode\/(.+)/, function (req, res, next) {
  var sourceFile = path.join(config.mediaRoot, req.params[0]);
  
  if (sourceFile.indexOf(config.mediaRoot) !== 0) {
    var err = new Error('Not Found');
    err.code = 404;
    return next(err);
  }
  
  if (!req.query.profile || !profiles[req.query.profile]) {
    var err = new Error('Transcoding profile not found');
    err.code = 400;
    return next(err);
  }
  
  var profile = profiles[req.query.profile]({
    inputFilePath: sourceFile
  });
  
  res.set('Content-Type', profile.contentType);
  
  var encoder;
  encoder = child_process.spawn(profile.exec, profile.params);
  encoder.stdout.pipe(res);

  encoder.stderr.on('data', function (data) {
    applog.debug('Encoder STDERR', data.toString());
  });

  encoder.on('close', function (code, signal) {
    applog[code ? 'warn' : 'debug']('Encoder process closed with code: ' + code);
    encoder = null;
  });
  
  encoder.on('error', function (err) {
    applog.error('Encoder error', err);
  });

});

app.get('*', function(req, res, next) {
  var err = new Error('Not Found');
  err.code = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.status(err.code || 500);
  res.render('error.jade', {
    error: err
  });
});

applog.info('Listening on ' + config.server.bindAddress + ':' + config.server.port);
app.listen(config.server.port, config.server.bindAddress);