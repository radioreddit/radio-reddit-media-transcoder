# Radio Reddit Media Transcoder
HTTP server that facilitates on-demand media conversion.

![Media Transcoder Flowchart](https://bitbucket.org/radioreddit/radio-reddit-media-transcoder/raw/master/doc/flow.svg)

The server accepts requests, fetches source media from disk, and executes an
external process to handle the actual encoding.  As that process outputs data,
the response data is streamed back to the client.  For most audio files, this
can happen in a timely manner as to allow clients to receive audio in whatever
format and codec they require without having to convert the media ahead of
time.

## Installation

1. `git clone`
1. `npm install`
1. Configure `conf/profiles.js`
1. Configure `conf/config.json`
1. Run with `node app.js`.

## Encoder Profiles (conf/profiles.js)
The media transcoder does not actually convert media on its own.  It requires
an external executable to do the encoding.  You can use FFmpeg, avconv, or
similar.  As long as your executable can handle outputting data via STDOUT, it
can be used with the media transcoder.

An encoding profile is a named predefined set of codec parameters.  It is
defined in `conf/profiles.js`.  `profiles.js` is expected to return an object
with keys naming each profile.  Each profile is defined as a function which
accepts an `options` object, and returns an object which allows the Media
Transcoder to execute your external encoder with the correct parameters.  For
example:

```JavaScript
var ffmpegPath = '/usr/local/bin/ffmpeg';

module.exports = {
  mp3_128k: function (options) {
    return {
      exec: ffmpegPath,
      contentType: 'audio/mpeg',
      params: ['-nostats', '-i', options.inputFilePath, '-acodec', 'mp3', '-f', 'mp3', '-ab', '128k', '-ar', '44100', '-ac', '2', '-']
    };
  }
}
```

In this example, `profiles.js` only returns a single encoding profile, named
`mp3_128k`.  When the Media Transcoder receives a request with the profile
named `mp3_128k`, it will execute this function, passing in an `option` object:

```JavaScript
{
  inputFilePath: '/media/some-file.flac'
}
```

This object currently only contains one element, `inputFilePath`, which defines
the file path on disk to be transcoded.  The profile function will then return
an object specifying the path to the binary to be executed, the `Content-Type`
of the media stream returned, and an array of parameters to be passed to the
encoder binary.  The encoder is expected to output the stream via STDOUT.

## General Configuration (conf/config.json)
Specify the media document root, port, and bind address in `conf/config.json`:

```JSON
{
  "mediaRoot": "/media",
  "server": {
    "bindAddress": "0.0.0.0",
    "port": 3000
  }
}
```

## Usage
Once the Media Transcoder is configured and running, access it via HTTP:

    http://transcoder.dev.radioreddit.com/transcode/path/to/media.flac?profile=mp3_128k
    
In this example, the media file would be located at `/media/path/to/media.flac`
and the profile for encoding would be `mp3_128k`.

## Notes and Caveats
Not all media formats are streamable.  MP4, for example, requires that the
encoder go back to the beginning of the file to add data after the entire file
is done being encoded.  This is not possible to do when the beginning of the
file has already been streamed to the client.

It is suggested to use a web server in a reverse proxy configuration (such as
Nginx) in front of the Media Transcoder so that you have better tools for
operational needs, such as logging, throttling requests, and caching media
that has already been transcoded.

## Contributing
Pull requests in line with the project's goals are generally accepted and
encouraged!  Please submit an issue in the issue tracker if you have questions
prior to working up a pull request.

A `Vagrantfile` is included in this repo to help development.  You can run it
with a simple `vagrant up`.  It is recommended that you use the [Vagrant Host
Manager](https://github.com/smdahlen/vagrant-hostmanager) to make development
easier.  If you use that plugin, you can access the Vagrant VM at
http://transcoder.dev.radioreddit.com.

You may want to add a mount for your media before starting the Vagrant VM.
Instructions for doing so can be found within the `Vagrantfile` itself.

Note that the application is not started automatically when the Vagrant box
is ran.  You can start it by running `node /vagrant/app.js`.

## License

The MIT License (MIT)

Copyright (c) 2014, Radio Reddit, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.